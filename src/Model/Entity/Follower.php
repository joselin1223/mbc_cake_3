<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Follower Entity
 *
 * @property int $id
 * @property string|null $from_to
 * @property int $user_id_from
 * @property int $user_id_to
 * @property int $is_deleted
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $created
 */
class Follower extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'from_to' => true,
        'user_id_from' => true,
        'user_id_to' => true,
        'is_deleted' => true,
        'modified' => true,
        'created' => true
    ];
}
