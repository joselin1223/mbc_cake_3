<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Post Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $parent_post_id
 * @property string $description
 * @property string $image
 * @property int $is_deleted
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\ParentPost $parent_post
 * @property \App\Model\Entity\PostComment[] $post_comments
 * @property \App\Model\Entity\PostLike[] $post_likes
 */
class Post extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'parent_post_id' => true,
        'description' => true,
        'image' => true,
        'is_deleted' => true,
        'modified' => true,
        'created' => true,
        'user' => true,
        'parent_post' => true,
        'post_comments' => true,
        'post_likes' => true
    ];
}
