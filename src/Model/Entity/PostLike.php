<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PostLike Entity
 *
 * @property int $id
 * @property string|null $post_user
 * @property int $post_id
 * @property int $user_id
 * @property int $is_deleted
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\Post $post
 * @property \App\Model\Entity\User $user
 */
class PostLike extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'post_user' => true,
        'post_id' => true,
        'user_id' => true,
        'is_deleted' => true,
        'modified' => true,
        'created' => true,
        'post' => true,
        'user' => true
    ];
}
