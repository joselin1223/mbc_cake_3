<?php

namespace App\Model\Table;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\PostCommentsTable&\Cake\ORM\Association\HasMany $PostComments
 * @property \App\Model\Table\PostLikesTable&\Cake\ORM\Association\HasMany $PostLikes
 * @property \App\Model\Table\PostsTable&\Cake\ORM\Association\HasMany $Posts
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany(
            'PostComments',
            [
                'foreignKey' => 'user_id'
            ]
        );
        $this->hasMany(
            'PostLikes',
            [
                'foreignKey' => 'user_id'
            ]
        );
        $this->hasMany(
            'Posts',
            [
                'foreignKey' => 'user_id'
            ]
        );
        $this->hasMany(
            'Followers',
            [
                'className' => 'Followers',
                'foreignKey' => 'user_id_to',
                'conditions' => ['is_deleted' => 0]
            ]
        );
        $this->hasMany(
            'Following',
            [
                'className' => 'Followers',
                'foreignKey' => 'user_id_from',
                'conditions' => ['is_deleted' => 0]
            ]
        );
        $this->belongsTo(
            'hasRelation',
            [
                'className' => 'Followers',
                'foreignKey' => false,
                'conditions' => [
                    'OR' => [
                        'hasRelation.user_id_from = Users.id',
                        'hasRelation.user_id_to = Users.id',
                    ],
                    'hasRelation.is_deleted' => 0
                ]
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');
        $validator
            ->scalar('username')
            ->maxLength('username', 45)
            ->notEmptyString('username', 'create')
            ->add(
                'username',
                [
                    'unique' => [
                        'rule' => 'validateUnique',
                        'provider' => 'table',
                        'message' => 'Already taken'
                    ],
                    'custom' => [
                        'rule' => ['custom', '/^[a-z0-9-_]*$/i'],
                        'message' => 'Alphabets, numbers, dash and underscore allowed'
                    ],
                    'length' => [
                        'rule' => ['minLength', 8],
                        'message' => 'Username need to be at least 8 characters long',
                    ]
                ]
            );
        $validator
            ->scalar('password')
            ->add(
                'password',
                [
                    'minLength' => [
                        'rule' => ['minLength', 8],
                        'last' => true,
                        'message' => 'Password need to be at least 8 characters long'
                    ],
                    'oneCaps' => [
                        'rule' => function ($value) {
                            $uppercase = 0;
                            $password = $value;
                            preg_match_all("/[A-Z]/", $password, $caps_match);
                            $caps_count = count($caps_match[0]);
                            /** Rutrun boolean */
                            return ($uppercase < $caps_count);
                        },
                        'message' => 'Password must have at least 1 capital letter'
                    ],
                    'oneLower' => [
                        'rule' => function ($value) {
                            $uppercase = 0;
                            $password = $value;
                            preg_match_all("/[a-z]/", $password, $caps_match);
                            $caps_count = count($caps_match[0]);
                            /** Rutrun boolean */
                            return ($uppercase < $caps_count);
                        },
                        'message' => 'Password must have at least 1 small case letter'
                    ],
                    'oneNumber' => [
                        'rule' => function ($value) {
                            $uppercase = 0;
                            $password = $value;
                            preg_match_all("/[0-9]/", $password, $caps_match);
                            $caps_count = count($caps_match[0]);
                            /** Rutrun boolean */
                            return ($uppercase < $caps_count);
                        },
                        'message' => 'Password must have at least 1 number'
                    ]
                ]
            )
            ->notEmptyString('password')
            ->add(
                'confirm_password',
                'custom',
                [
                    'rule' => function ($value, $context) {
                        $data = false;
                        if ($value == $context['data']['password']) {
                            $data = true;
                        }
                        /** Rutrun boolean */
                        return $data;
                    },
                    'message' => 'Password not match'
                ]
            );
        /**
         * Fullname
         */
        $validator
            ->scalar('fullname')
            ->maxLength('fullname', 45)
            ->notEmptyString('fullname')
            ->add(
                'fullname',
                [
                    'minLength' => [
                        'rule' => ['minLength', 4],
                        'last' => true,
                        'message' => 'Fullname need to be at least 8 characters long'
                    ],
                    'validChar' => [
                        'rule' => ['custom', "/^[a-zA-Z .'-]*$/i"],
                        'message' => "Alphabetical characters, spaces, colons,dashes,and dot are allowed",
                    ],
                    'notBlank' => [
                        'rule' => 'notBlank',
                        'message' => 'Fullname can not be empty',
                        'last' => true
                    ],
                ]
            );
        /**
         * Age
         */
        $validator
            ->integer('age')
            ->notEmptyString('age')
            ->add(
                'age',
                [
                    'verifyAge' => [
                        'rule' => function ($value) {
                            $return = false;
                            if ($value != '') {
                                if ($value > 12 && $value < 91) {
                                    $return = true;
                                } else {
                                    $return = false;
                                }
                            }
                            /** Rutrun boolean */
                            return $return;
                        },
                        'message' => 'Age must be greater than 13 and less than 90'
                    ]
                ]
            );
        /**
         * Address
         */
        $validator
            ->scalar('address')
            ->maxLength('address', 45)
            ->notEmptyString('address')
            ->add(
                'address',
                [
                    'minLength' => [
                        'rule' => ['minLength', 8],
                        'last' => true,
                        'message' => 'Address need to be at least 8 characters long'
                    ],
                    'validAddress' => [
                        'rule' => ['custom', '/^[a-z0-9 .]*$/i'],
                        'message' => 'Alphanumeric characters with spaces and dot only',
                    ],
                    'notBlank' => [
                        'rule' => 'notBlank',
                        'message' => 'Address can not be empty',
                        'last' => true
                    ],
                ]
            );
        /**
         * Email
         */
        $validator
            ->email('email')
            ->notEmptyString('email')
            ->maxLength('address', 255)
            ->add(
                'email',
                [
                    'unique' => [
                        'rule' => 'validateUnique',
                        'provider' => 'table',
                        'message' => 'Already taken'
                    ],
                    'validEmail' => [
                        'rule' => ['custom', '/^[a-z0-9.@_-]*$/i'],
                        'message' => 'Alphanumeric characters, dot, @, - and underscore are allowed',
                    ]

                ]
            );
        /**
         * Image
         */
        $validator
            ->allowEmptyFile('image')
            ->add(
                'image',
                [
                    'extension' => [
                        'rule' => function ($value, $context) {
                            $return = true;
                            $fileData = pathinfo($value['name']);
                            $ext = $fileData['extension'];
                            $allowExtension = ['gif', 'GIF', 'jpeg', 'JPEG', 'png', 'PNG', 'jpg', 'JPG'];
                            if (in_array($ext, $allowExtension)) {
                                $return = true;
                            } else {
                                $return = false;
                            }
                            /** Rutrun boolean */
                            return $return;
                        },
                        'message' => 'Invalid file type'
                    ],
                ]
            );
        /**
         * Bio
         */
        $validator
            ->scalar('bio')
            ->maxLength('bio', 140)
            ->notEmptyString('bio')
            ->add(
                'bio',
                [
                    'notBlank' => [
                        'rule' => 'notBlank',
                        'message' => 'Bio can not be empty',
                        'last' => true
                    ],
                ]
            );
        /**
         * activation_code
         */
        $validator
            ->scalar('activation_code')
            ->allowEmptyFile('activation_code');
        /**
         * Validate the current password
         */
        $validator
            ->add(
                'current_password',
                'custom',
                [
                    'rule' => function ($value, $context) {
                        $id = $context['data']['id'];
                        $user = $this->get($id);
                        if ($user) {
                            if ((new DefaultPasswordHasher)->check($value, $user->password)) {
                                return true;
                            }
                        }
                        /** Rutrun boolean */
                        return false;
                    },
                    'message' => 'The old password does not match the current password!',
                ]
            )
            ->notEmpty('current_password');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
