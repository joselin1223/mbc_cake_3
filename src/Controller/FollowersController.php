<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Followers Controller
 *
 * @property \App\Model\Table\FollowersTable $Followers
 *
 * @method \App\Model\Entity\Follower[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FollowersController extends AppController
{
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->loadModel('Users');
        $execute = new Executes;
        $userSession = $execute->getUserSession('id');
        $searchUser = '';
        $searchUser = $this->request->session()->read('search_user');
        if (null !== $this->request->getData('search_user')) {
            $this->request->session()->write('search_user', $this->request->getData('search_user'));
            $searchUser = $this->request->session()->read('search_user');
        }

        $this->paginate = [
            'contain' => ['Followers'],
            'limit' => 4,
            'order' => [
                'Users.created' => 'DESC'
            ],
            'conditions' => [
                'Users.id !=' => $userSession,
                'Users.fullname LIKE' => "%" . $searchUser . "%",
                'Users.activated' => ACTIVATED
            ]
        ];
        $userDetail = $this->paginate($this->Users);
        $this->set(compact('userDetail', 'searchUser'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $follower = $this->Followers->newEntity();
        if ($this->request->is('post')) {
            $follower = $this->Followers->patchEntity($follower, $this->request->getData());
            if ($this->Followers->save($follower)) {
                $this->Flash->success(__('The follower has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The follower could not be saved. Please, try again.'));
        }
        $this->set(compact('follower'));
    }

    /**
     * Add follow
     *
     * @return $this->response
     */
    public function addFollow()
    {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $execute = new Executes;
        $userIdFrom = $execute->getUserSession('id');
        $userIdTo = $this->request->getData('user_id_to');
        $fromTo = $userIdFrom . '_' . $userIdTo;
        $formData = [
            'from_to' => $fromTo,
            'user_id_from' => $userIdFrom,
            'user_id_to' => $userIdTo
        ];
        $follow = $this->Followers->newEntity();
        $follow = $this->Followers->patchEntity($follow, $formData);
        $return = '';
        if ($this->Followers->save($follow)) {
            $return = 'followed';
        } else {
            $return = $this->follow($fromTo);
        }
        /** This return string only */
        return $this->response
            ->withType("application/json")
            ->withStringBody(json_encode($return));
    }

    /**
     * Like function
     *
     * @param [int] $fromTo -> user to be followed ot unfollowed
     * @return $return
     */
    public function follow($fromTo)
    {
        $return = '';
        $follow = $this->Followers
            ->findByFromTo($fromTo)
            ->first();
        if ($follow->is_deleted) {
            $follow->is_deleted = FOLLOW;
            $return = 'followed';
        } else {
            $follow->is_deleted = UNFOLLOW;
            $return = 'unfollowed';
        }
        if ($this->Followers->save($follow)) {
            /** This return string only */
            return $return;
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Follower id.
     * @return void
     */
    public function edit($id = null)
    {
        $follower = $this->Followers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $follower = $this->Followers->patchEntity($follower, $this->request->getData());
            if ($this->Followers->save($follower)) {
                $this->Flash->success(__('The follower has been saved.'));
                $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The follower could not be saved. Please, try again.'));
        }
        $this->set(compact('follower'));
    }
}
