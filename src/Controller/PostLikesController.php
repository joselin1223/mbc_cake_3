<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * PostLikes Controller
 *
 * @property \App\Model\Table\PostLikesTable $PostLikes
 *
 * @method \App\Model\Entity\PostLike[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostLikesController extends AppController
{
    /**
     * Insert like
     *
     * @return $this->response
     */
    public function insertLike()
    {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $execute = new Executes;
        if ($this->request->is('post')) {
            $postId = $this->request->getData('post_id');
            $userId = $execute->getUserSession('id');
            $formData = [
                'post_user' => $postId . '_' . $userId,
                'post_id' => $postId,
                'user_id' => $userId,

            ];
            $postLike = $this->PostLikes->newEntity();
            $postLike = $this->PostLikes->patchEntity($postLike, $formData);
            $status = true;
            if ($postLike->hasErrors()) {
                $status = $this->like($postId);
            } else {
                if ($this->PostLikes->save($postLike)) {
                    $status = true;
                }
            }
            if ($status) {
                $status = $this->countLike($postId);
            }
            /** Return boolean status*/
            return $this->response
                ->withType("application/json")
                ->withStringBody(json_encode($status));
        }
    }

    /**
     * Like function
     *
     * @param [int] $postId -> post if id to be like
     * @return $status
     */
    public function like($postId)
    {
        $execute = new Executes;
        $userId = $execute->getUserSession('id');
        $postUser = $postId . '_' . $userId;
        $postLike = $this->PostLikes
            ->findByPostUser($postUser)
            ->first();
        if ($postLike->is_deleted) {
            $postLike->is_deleted = LIKE;
        } else {
            $postLike->is_deleted = UNLIKE;
        }
        $status = true;
        if ($this->PostLikes->save($postLike)) {
            $status = true;
        } else {
            $status = false;
        }
        /** Return boolean */
        return $status;
    }

    /**
     * This function is used to count the like in the post
     *
     * @param [int] $postId -> post if id to be count
     * @return $postLike
     */
    public function countLike($postId = null)
    {
        $postLike = $this->PostLikes
            ->findByPostId($postId)
            ->where(['is_deleted' => LIKE])
            ->count('*');
        /** Return boolean status*/
        return $postLike;
    }
}
