<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    /**
     * This function limit the user accessibility
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(
            [
                'add',
                'activateAccount',
                'activateSuccess',
                'logout',
                'login',
                'logout',
                'register'
            ]
        );
    }

    /**
     * Check ig the user already login
     *
     * @return void
     */
    public function checkIfAlreadyLogin()
    {
        if ($this->Auth->user()) {
            $this->redirect(
                [
                    'controller' => 'Posts',
                    'action' => 'index'
                ]
            );
        }
    }

    /**
     * Login the user
     *
     * @return void
     */
    public function login()
    {
        $this->checkIfAlreadyLogin();
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                if ($user['activated'] == IS_NOT_ACTIVATED) {
                    $this->Flash->error('Please activate your account first.');
                    $this->redirect($this->Auth->logout());
                } else {
                    $this->Auth->setUser($user);
                    $this->Flash->success('Welcome ' . $user['fullname'] . '  in MicroBlog Chaste.');
                    $this->redirect(
                        [
                            'controller' => 'Posts',
                            'action' => 'index'
                        ]
                    );
                }
            } else {
                $this->Flash->error('Login credentials are incorrect!');
            }
        }
    }

    /**
     * Logout the user
     *
     * @return void
     */
    public function logout()
    {
        $this->Flash->success('Thank you, Come visit us again.');
        $this->redirect($this->Auth->logout());
    }

    /**
     * This allow the user to register
     *
     * @return void
     */
    public function register()
    {
        $execute = new Executes;
        $this->checkIfAlreadyLogin();
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $imgNameWillBeUse = uniqid();
            $activationCode = uniqid();
            $formData = $this->request->getData();
            if ($formData['image']['name'] != '') {
                $formData['image']['name'] = $execute->getUniqueFileName($imgNameWillBeUse, $formData['image']['name']);
            } else {
                $formData['image']['name'] = 'profile.png';
            }
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if (!($user->hasErrors())) {
                if (!($execute->uploadImage($formData['image'], $formData['image']['name'], 'profile')) && $formData['image']['name'] != 'profile.png') {
                    $this->Flash->error(__('The File is too large'));
                }
                $this->mail($user['email'], $activationCode, $user['fullname']);
            }
            $user['image'] = $formData['image']['name'];
            $user['activation_code'] = $activationCode;
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Check your email to activate your account'));
                $this->redirect(['action' => 'login']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
    }

    /**
     * This is used to send activation link via email
     *
     * @param [string] $emailAddress -> email address of receiver
     * @param [string] $activationCode -> uniqid
     * @param [string] $fullname -> User name
     * @return void
     */
    public function mail($emailAddress, $activationCode, $fullname)
    {
        try {
            $host = $this->request->host();
            $message = [
                'link' => "http://" . $host . "/Users/activateAccount/" . $emailAddress . "/" . $activationCode,
                'fullname' => $fullname
            ];
            $email = new Email('default');
            $email->transport('mail');
            $email
                ->template('activation', null)
                ->emailFormat('html')
                ->from(['MicroBlog.chaste@email.com.ph' => 'MicroBlog Chaste'])
                ->to($emailAddress)
                ->cc('macayananjoselin@gmail.com')
                ->subject('Activate account in MicroBlog Chaste')
                ->send(json_encode($message));
        } catch (\Throwable $th) {
            die('Email not sent');
        }
    }

    /**
     * This will activate the user account
     *
     * @param [type] $email -> email address to be activated
     * @param [type] $activateCode -> uniqId
     * @return void
     */
    public function activateAccount($email, $activateCode)
    {
        if (!$email || !$activateCode) {
            throw new NotFoundException(__('Invalid post'));
        }
        $user = $this->Users
            ->findByEmail($email)
            ->where(
                [
                    'activation_code' => $activateCode,
                    'activated' => IS_NOT_ACTIVATED
                ]
            )
            ->first();
        if ($user != null) {
            $user->activated = IS_ACTIVATED;
            $user->activation_code = uniqid();
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Your account has been successfully activated!'));
            }
        } else {
            $this->redirect(
                [
                    'controller' => 'Error',
                    'action' => 'customPageNotFound'
                ]
            );
        }
    }

    /**
     * Profile page
     *
     * @param [int] $id -> current user visited
     * @return void
     */
    public function profile($id = null)
    {
        $profilePage = $this->getCurrentUser($id);
        if ($profilePage == null) {
            $this->redirect(
                [
                    'controller' => 'Error',
                    'action' => 'customPageNotFound'
                ]
            );
        }
        $this->loadModel('Posts');
        $execute = new Executes;
        $posts = $this->Posts->newEntity();
        if ($this->request->is('post')) {
            $imgNameWillBeUse = uniqid();
            $formData = $this->request->getData();
            $formData['user_id'] = $execute->getUserSession('id');
            if ($formData['image']['name'] != '') {
                $formData['image']['name'] = $execute->getUniqueFileName($imgNameWillBeUse, $formData['image']['name']);
            } else {
                $formData['image']['name'] = '';
            }
            $posts = $this->Posts->patchEntity($posts, $formData);
            if (!($posts->hasErrors())) {
                if ($formData['image']['name'] != 'profile.png' && $formData['image']['name'] != '') {
                    if (!($execute->uploadImage($formData['image'], $formData['image']['name'], 'post'))) {
                        $this->Flash->error(__('The File is too large'));
                    }
                }
            }
            $posts['image'] = $formData['image']['name'];
            if ($this->Posts->save($posts)) {
                $this->Flash->success(__('The post has been saved.'));
                $this->redirect(['action' => 'profile', $id]);
            } else {
                $this->Flash->error(__('The post could not be saved. Please, try again.'));
            }
        }
        $search = $this->request->session()->read('search_post');
        if (null !== $this->request->getData('search_post_description')) {
            $this->request->session()->write('search_post', $this->request->getData('search_post_description'));
            $search = $this->request->session()->read('search_post');
        }
        $this->paginate = [
            'contain' => ['Users', 'PostLikes'],
            'limit' => MAXIMUM_POST_CAN_BE_SHOW,
            'order' => [
                'Posts.id' => 'desc'
            ],
            'conditions' => [
                'Posts.is_deleted' => IS_NOT_DELETED,
                'Posts.user_id' => $id,
                'Posts.description LIKE' => "%" . $search . "%"
            ]
        ];
        $postsDetail = $this->paginate($this->Posts);
        $noValue = true;
        foreach ($postsDetail as $postTotal) {
            $noValue = false;
        }
        if ($noValue && $search != '') {
            $this->Flash->error(__('No result found in "' . h($search) . '"'));
        }
        $totalFollower = $this->countField('Followers', $id);
        $totalFollowing = $this->countField('Following', $id);
        $this->set(compact('profilePage', 'posts', 'postsDetail', 'search', 'totalFollower', 'totalFollowing'));
    }

    /**
     * Get current user for profile page
     *
     * @param [int] $id -> current user
     * @return $user
     */
    public function getCurrentUser($id)
    {
        $user = $this->Users
            ->findById($id)
            ->first();
        /**
         * Return the user information of the user
         */
        return $user;
    }

    /**
     * This function update user information
     *
     * @return void
     */
    public function updateUser()
    {
        $execute = new Executes;
        $userSession = $execute->getUserSession('id');
        $userUsed = $this->Users->get($userSession);
        $imgNameWillBeUse = uniqid();
        $formData = $this->request->getData();
        if ($this->request->is('post')) {
            $userUsed = $this->Users->patchEntity($userUsed, $formData);
            if ($userUsed->isDirty()) {
                if ($this->Users->save($userUsed)) {
                    $this->Auth->setUser($userUsed);
                    $this->Flash->success(__('Your account has been updated successfully!'));
                }
            }
            if ($userUsed->hasErrors()) {
                $this->Flash->error(__('Please check the inputted fields!'));
            }
        }
        $this->set(compact('userUsed'));
    }

    /**
     * Update profile image
     *
     * @return void
     */
    public function updateProfilePic()
    {
        $execute = new Executes;
        $userSession = $execute->getUserSession('id');
        $userUsed = $this->Users->get($userSession);
        if ($this->request->is('post')) {
            $formData = $this->request->getData();
            if ($formData['image']['name'] == '' || $formData['image']['name'] == 'profile.png') {
                $this->Flash->error(__('Please insert image first!'));
            } else {
                $imgNameWillBeUse = uniqid();
                $userUsed = $this->Users->patchEntity($userUsed, $formData);
                $imgName = $execute->getUniqueFileName($imgNameWillBeUse, $formData['image']['name']);
                if (!($execute->uploadImage($formData['image'], $imgName, 'profile'))) {
                    $this->Flash->error(__('The File is too large'));
                } else {
                    $userUsed->image = $imgName;
                }
                if ($userUsed->isDirty()) {
                    if ($this->Users->save($userUsed)) {
                        $this->Auth->setUser($userUsed);
                        $this->Flash->success(__('Your account has been updated successfully!'));
                    }
                }
                if ($userUsed->hasErrors()) {
                    $this->Flash->error(__('Please check the inputted fields!'));
                }
            }
        }
        $this->set(compact('userUsed'));
    }

    /**
     * Remove Profile Image
     *
     * @return void
     */
    public function removeProfileImage()
    {
        $execute = new Executes;
        $userSession = $execute->getUserSession('id');
        $userUsed = $this->Users->get($userSession);
        $userUsed->image = 'profile.png';
        if ($this->Users->save($userUsed)) {
            $this->Auth->setUser($userUsed);
            $this->redirect(
                [
                    'action' => 'updateProfilePic'
                ]
            );
        }
    }

    /**
     * This function is used to change the user password
     *
     * @return void
     */
    public function changePassword()
    {
        $execute = new Executes;
        $password = $this->Users->newEntity();
        $userSession = $execute->getUserSession('id');
        if ($this->request->is('post')) {
            $dataSend = $this->request->getData();
            $user = $this->Users
                ->findById($userSession)
                ->first();
            $formData = [
                'current_password' => $dataSend['current_password'],
                'id' => $userSession,
                'password' => $dataSend['password'],
                'confirm_password' => $dataSend['confirm_password']
            ];
            $password = $this->Users->patchEntity($password, $formData);
            if (!($password->hasErrors())) {
                $user->password = $dataSend['password'];
                if ($this->Users->save($user)) {
                    $this->Auth->setUser($user);
                    $this->Flash->success(__('Your account password has been updated successfully!'));
                } else {
                    $this->Flash->error(__('Please check the errors of the inputted fields!'));
                }
            } else {
                $this->Flash->error(__('Please check the errors of the inputted fields!'));
            }
        }
        $this->set(compact('password'));
    }

    /**
     * Show users following
     *
     * @param [int] $id -> current user visited in the profile page
     * @return void
     */
    public function following($id = null)
    {
        $search = $this->request->session()->read('search_following');
        if (null !== $this->request->getData('search_following')) {
            $this->request->session()->write('search_following', $this->request->getData('search_following'));
            $search = $this->request->session()->read('search_following');
        }
        $this->paginate = [
            'contain' => ['hasRelation'],
            'limit' => 4,
            'order' => [
                'Users.created' => 'DESC'
            ],
            'conditions' => [
                'Users.id !=' => $id,
                'hasRelation.user_id_from' => $id,
                'Users.fullname LIKE' => "%" . $search . "%",
                'Users.activated' => ACTIVATED
            ]
        ];
        $userDetail = $this->paginate($this->Users);
        $userFollower = $userDetail;
        $userOwner = $id;
        $this->set(compact('userFollower', 'userOwner', 'search'));
    }

    /**
     * Show users follower
     *
     * @param [int] $id -> current user visited in the proile page
     * @return void
     */
    public function follower($id = null)
    {
        $search = $this->request->session()->read('search_follower');
        if (null !== $this->request->getData('search_follower')) {
            $this->request->session()->write('search_follower', $this->request->getData('search_follower'));
            $search = $this->request->session()->read('search_follower');
        }
        $this->paginate = [
            'contain' => ['hasRelation'],
            'limit' => 4,
            'order' => [
                'Users.created' => 'DESC'
            ],
            'conditions' => [
                'Users.id !=' => $id,
                'hasRelation.user_id_to' => $id,
                'Users.fullname LIKE' => "%" . $search . "%",
                'Users.activated' => ACTIVATED
            ]
        ];
        $userDetail = $this->paginate($this->Users);
        $userFollowing = $userDetail;
        $userOwner = $id;
        $this->set(compact('userFollowing', 'userOwner', 'search'));
    }

    /**
     * Get the total follower and follower
     *
     * @param [string] $field -> followers or following
     * @param [int] $id -> current user visited in the profile page
     * @return $userCount
     */
    public function countField($field = null, $id = null)
    {
        $query = $this->Users->find('all');
        $userDetail = $query
            ->contain($field)
            ->where(
                [
                    'Users.id !=' => $id,
                    'Users.activated' => IS_ACTIVATED,
                ]
            )
            ->order(['Users.created' => 'DESC'])
            ->all();
        $userCount = 0;
        foreach ($userDetail as $user) {
            if ($field == 'Following') {
                if (($user->following != [])) {
                    foreach ($user->following as $following) {
                        if ($following->user_id_to == $id) {
                            $userCount++;
                        }
                    }
                }
            } else {
                if (!($user->followers == [])) {
                    foreach ($user->followers as $follower) {
                        if ($follower->user_id_from == $id) {
                            $userCount++;
                        }
                    }
                }
            }
        }
        /**
         * Return the total number of follower or following of the user
         */
        return $userCount;
    }

    /**
     * Check if followed
     *
     * @param [int] $id -> user
     * @return void
     */
    public function checkIfFollowed($id = null)
    {
        $execute = new Executes;
        $visitedUser = $id;
        $userSession = $execute->getUserSession('id');
        $query = $this->Users->find('all');
        $userDetail = $query
            ->contain('Followers')
            ->where(
                [
                    'Users.id !=' => $userSession,
                    'Users.activated' => IS_ACTIVATED,
                ]
            )
            ->order(['Users.created' => 'DESC'])
            ->all();
        $userCount = 0;
        foreach ($userDetail as $user) {
            if (!($user->followers == [])) {
                foreach ($user->followers as $follower) {
                    if ($follower->user_id_from == $userSession && $follower->user_id_to == $id) {
                        $userCount++;
                    }
                }
            }
        }
        $this->set(compact('userCount', 'visitedUser'));
    }
}
