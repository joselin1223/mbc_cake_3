<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * PostComments Controller
 *
 * @property \App\Model\Table\PostCommentsTable $PostComments
 *
 * @method \App\Model\Entity\PostComment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostCommentsController extends AppController
{
    /**
     * Fetch all post comment
     *
     * @param [int] $item -> pagination limit
     * @return void
     */
    public function viewComments($item)
    {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        if ($this->request->is('post')) {
            if ($item == 0) {
                $item = 3;
            }
            $post_id = $this->request->getData('post_id');
            $query = $this->PostComments->find('all')
                ->where(['PostComments.post_id' => $post_id])
                ->contain('Users')
                ->order(['PostComments.id' => 'DESC'])
                ->limit($item);
            $result = $query->all();
            $content = json_encode($result);
            $this->response = $this->response->withStringBody($content);
        }
    }

    /**
     * Add method
     *
     * @return $this->response
     */
    public function add()
    {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $formData = [
                'description' => $this->request->getData('description'),
                'user_id' => $this->getUserSession('id'),
                'post_id' => (int)$this->request->getData('post_id')
            ];
            $postComment = $this->PostComments->newEntity();
            $postComment = $this->PostComments->patchEntity($postComment, $formData);
            $errorMessage = '';
            if ($this->PostComments->save($postComment)) {
                $errorMessage = true;
            } else {
                try {
                    if (array_key_exists('notBlank', $postComment->errors('description'))) {
                        $errorMessage = $postComment->errors('description')['notBlank'];
                    }
                    if (array_key_exists('maxLength', $postComment->errors('description'))) {
                        $errorMessage = 'Maximum characters are 140 only';
                    }
                } catch (\Throwable $th) {
                    $errorMessage = 'Something went wrong!';
                }
                if ($errorMessage == '') {
                    $errorMessage = 'Please check the description input fields';
                }
            }
            /** Return boolean status if true and message if false*/
            return $this->response
                ->withType("application/json")
                ->withStringBody(json_encode($errorMessage));
        }
    }

    /**
     * Get user session
     *
     * @param [string] $field -> get session according to the field
     * @return $session
     */
    public function getUserSession($field)
    {
        $session = $this->Auth->user($field);
        /** Return the session according to the field */
        return $session;
    }
}
