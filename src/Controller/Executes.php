<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Executes
 * Repeatedly used in other controllers
 */
class Executes extends AppController
{
    /**
     * This is used to upload image in Post and profile
     *
     * @param [type] $imgObj -> image object
     * @param [type] $imgNameWillBeUse -> unique name of the image
     * @param [type] $field -> table field
     * @return $return
     */
    public function uploadImage($imgObj, $imgNameWillBeUse, $field = 'post')
    {
        $return = false;
        if ($imgObj['tmp_name'] <= MAX_SIZE) {
            $imgName = $imgNameWillBeUse;
            $imgTemp = $imgObj['tmp_name'];
            $imgPath = "img/blog/" . $field . "/" . $imgName;
            if (move_uploaded_file($imgTemp, WWW_ROOT . $imgPath)) {
                $return = true;
            } else {
                $return = false;
            }
        } else {
            $return = false;
        }
        /** Return boolean */
        return $return;
    }

    /**
     * Get user session
     *
     * @param [type] $field -> user table field
     * @return $session
     */
    public function getUserSession($field)
    {
        $session = $this->Auth->user($field);
        /* return a specific field of session */
        return $session;
    }

    /**
     * Set a unique filename for image
     *
     * @param [type] $imgName -> unique image name
     * @param [type] $filename -> image name
     * @return $data
     */
    public function getUniqueFileName($imgName, $filename)
    {
        $tmp = explode(".", $filename);
        $extension = end($tmp);
        $data = $imgName . '.' . $extension;
        /** Return the file name with extension type */
        return $data;
    }
}
