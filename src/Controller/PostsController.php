<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 *
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostsController extends AppController
{
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $execute = new Executes;
        $posts = $this->Posts->newEntity();
        $userSession = $execute->getUserSession('id');
        $postPage = true;
        if ($this->request->is('post')) {
            $imgNameWillBeUse = uniqid();
            $execute = new Executes;
            $formData = $this->request->getData();
            $formData['user_id'] = $userSession;
            if ($formData['image']['name'] != '') {
                $formData['image']['name'] = $execute->getUniqueFileName($imgNameWillBeUse, $formData['image']['name']);
            } else {
                $formData['image']['name'] = '';
            }
            $posts = $this->Posts->patchEntity($posts, $formData);
            if (!($posts->hasErrors()) && $formData['image']['name'] != '' && !($execute->uploadImage($formData['image'], $formData['image']['name']))) {
                $this->Flash->error(__('The File is too large'));
            }
            $posts['image'] = $formData['image']['name'];
            if ($this->Posts->save($posts)) {
                $this->Flash->success(__('The post has been saved.'));
                $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The post could not be saved. Please, try again.'));
            }
        }
        $search = $this->request->session()->read('search_post');
        if (null !== $this->request->getData('search_post_description')) {
            $this->request->session()->write('search_post', $this->request->getData('search_post_description'));
            $search = $this->request->session()->read('search_post');
        }

        $query = $this->Posts->find('all');
        $postTemp = $query
            ->contain(['Users', 'PostLikes', 'hasRelation'])
            ->where(
                [
                    'Posts.is_deleted' => IS_NOT_DELETED,
                    'hasRelation.is_deleted' => IS_NOT_DELETED,
                    'hasRelation.user_id_from' => $userSession,
                    'Posts.description LIKE' => "%" . $search . "%",
                ]
            )
            ->limit(MAXIMUM_POST_CAN_BE_SHOW)
            ->all();

        $noValue = true;
        foreach ($postTemp as $postRelation) {
            $noValue = false;
        }
        if (!($noValue)) {
            $this->paginate = [
                'contain' => ['Users', 'PostLikes', 'hasRelation'],
                'limit' => MAXIMUM_POST_CAN_BE_SHOW,
                'order' => [
                    'Posts.id' => 'desc'
                ],
                'group' => ['Posts.id'],
                'conditions' => [
                    'Posts.is_deleted' => IS_NOT_DELETED,
                    'hasRelation.is_deleted' => IS_NOT_DELETED,
                    'hasRelation.user_id_from' => $userSession,
                    'Posts.description LIKE' => "%" . $search . "%",
                ],
            ];
        } else {
            $this->paginate = [
                'contain' => ['Users', 'PostLikes'],
                'limit' => MAXIMUM_POST_CAN_BE_SHOW,
                'order' => [
                    'Posts.id' => 'desc'
                ],
                'conditions' => [
                    'Posts.is_deleted' => IS_NOT_DELETED,
                    'Posts.user_id' => $userSession,
                    'Posts.description LIKE' => "%" . $search . "%"
                ]
            ];
        }
        $noValue = true;
        foreach ($this->paginate() as $postRelation) {
            $noValue = false;
        }

        if ($noValue && $search != '') {
            $this->Flash->error(__('No result found in "' . h($search) . '"'));
        }
        $postsDetail = $this->paginate($this->Posts);
        $this->set(compact('posts', 'postsDetail', 'postPage', 'search'));
    }

    /**
     * This is used edit the post
     *
     * @param [int] $id -> current post
     * @return void
     */
    public function updatePost($id = null)
    {
        $execute = new Executes;
        $updatePost = true;
        $userSession = $execute->getUserSession('id');
        $posts = $this->Posts
            ->findById($id)
            ->where(
                [
                    'user_id' => $userSession,
                    'is_deleted' => IS_NOT_DELETED
                ]
            )
            ->first();
        if ($posts == '') {
            $this->redirect(['controller' => 'Error', 'action' => 'customPageNotFound']);
        }
        if ($this->request->is('post')) {
            $temp = $posts;
            $posts = $this->Posts->newEntity();
            $formData = $this->request->getData();
            $posts = $this->Posts->patchEntity($posts, $formData);
            if ($formData['image']['name'] == '') {
                $posts->image = $temp->image;
            } else {
                $posts->image = 'true';
            }
            if ($temp->description != $this->request->getData('description') || $temp->image != $posts->image) {
                if (!($posts->hasErrors())) {
                    if ($posts->image != $temp->image) {
                        $imgNameWillBeUse = uniqid();
                        $tempImage = $execute->getUniqueFileName($imgNameWillBeUse, $formData['image']['name']);
                        if (!($execute->uploadImage($formData['image'], $tempImage, 'post'))) {
                            $this->Flash->error(__('The File is too large'));
                        } else {
                            $temp->image = $tempImage;
                        }
                    }
                    $temp->description = $posts->description;
                    if ($this->Posts->save($temp)) {
                        $this->Flash->success(__('Your post has been updated successfully!'));
                        $this->redirect(['action' => 'updatePost', $id]);
                    }
                }
            }
        }
        $this->set(compact('posts', 'updatePost'));
    }

    /**
     * Share post
     *
     * @param [int] $id -> post id to be shared
     * @return void
     */
    public function sharePost($id)
    {
        if ($id != $this->request->getData('parent_post_id')) {
            $this->Flash->error(__('Invalid action request'));
            $this->redirect(['action' => 'index']);
        }
        $execute = new Executes;
        $userId = $execute->getUserSession('id');
        $formData = [
            'user_id' => $userId,
            'parent_post_id' => $id,
            'description' => $this->request->getData('update_description')
        ];
        $posts = $this->Posts->newEntity();
        $posts = $this->Posts->patchEntity($posts, $formData);
        if ($this->Posts->save($posts)) {
            $this->Flash->success(__('Shared successfully!'));
            $this->redirect(['action' => 'index']);
        } else {
            $errorMessage = '';
            try {
                if (array_key_exists('notBlank', $posts->errors('description'))) {
                    $errorMessage = $posts->errors('description')['notBlank'];
                }
                if (array_key_exists('maxLength', $posts->errors('description'))) {
                    $errorMessage = $posts->errors('description')['maxLength'];
                }
            } catch (\Throwable $th) {
                $errorMessage = 'Something went wrong';
            }
            if ($errorMessage == '') {
                $errorMessage = 'Please check the description input fields';
            }
            $this->Flash->error(__($errorMessage));
            $this->redirect(['action' => 'index']);
        }
    }

    /**
     * Get parent post information
     *
     * @param [int] $id -> id of the post shared
     * @param [int] $originId -> which post contain the parent post
     * @param [int] $ownerId -> who shared the post
     * @return void
     */
    public function getParentPost($id = null, $originId = null, $ownerId = null)
    {
        if ($id != null) {
            $parentPost = $this->Posts
                ->findById($id)
                ->contain(['Users', 'PostLikes'])
                ->first();
            $this->set(compact('parentPost', 'originId', 'ownerId'));
        }
    }

    /**
     * Delete Shared Post
     *
     * @return $this->response
     */
    public function deleteShared()
    {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $parentPostId = $this->request->getData('parent_post_id');
        $execute = new Executes;
        $id = $this->request->getData('id');
        $userId = $execute->getUserSession('id');
        $post = $this->Posts
            ->findById($id)
            ->where(
                [
                    'parent_post_id' => $parentPostId,
                    'user_id' => $userId
                ]
            )
            ->first();
        $status = ['status' => true];
        if ($post != null) {
            $post->parent_post_id = null;
            $this->Posts->save($post);
            $status = ['status' => true];
        } else {
            $status = ['status' => false];
        }
        /** Return boolean status */
        return $this->response
            ->withType("application/json")
            ->withStringBody(json_encode($status));
    }

    /**
     * Remove image in the update post
     *
     * @param [int] $id -> id of the post
     * @return void
     */
    public function removeImage($id)
    {
        $execute = new Executes;
        $userSession = $execute->getUserSession('id');
        $post = $this->Posts
            ->findById($id)
            ->where(
                [
                    'user_id' => $userSession,
                    'is_deleted' => IS_NOT_DELETED
                ]
            )
            ->first();
        $post->image = '';
        $this->Posts->save($post);
        $this->redirect(['action' => 'updatePost', $id]);
    }

    /**
     * This post used to soft delete the post
     *
     * @return $this->response
     */
    public function delete()
    {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $execute = new Executes;
            $postId = $this->request->getData('id');
            $userId = $execute->getUserSession('id');
            $post = $this->Posts
                ->findById($postId)
                ->where(
                    [
                        'user_id' => $userId,
                        'is_deleted' => IS_NOT_DELETED
                    ]
                )
                ->first();
            $status = ['status' => true];
            if ($post == '') {
                $status = ['status' => 'invalid'];
            } else {
                $post->is_deleted = DELETED;
                if ($this->Posts->save($post)) {
                    $status = ['status' => true];
                } else {
                    $status = ['status' => false];
                }
            }
            /** Return boolean status and string */
            return $this->response
                ->withType("application/json")
                ->withStringBody(json_encode($status));
        }
    }

    /**
     * This post used to soft delete the post
     *
     * @param [id] $id post_id
     * @return $this->response
     */
    public function showToBeShare($id = null)
    {
        $postId = $id;
        $post = $this->Posts
            ->findById($postId)
            ->contain('Users')
            ->where(
                [
                    'is_deleted' => IS_NOT_DELETED
                ]
            )
            ->first();
        /** Return JSON data */
        return $this->response
            ->withType("application/json")
            ->withStringBody(json_encode($post));
    }

    /**
     * This is only used to show that the post is not exist
     *
     * @param [string] $search data search
     * @return void
     */
    public function noResultFound($search)
    {
        $this->set('search', $search);
    }

    /**
     * View the post
     *
     * @param [int] $id post id
     * @return void
     */
    public function view($id = null)
    {
        $execute = new Executes;
        $userSession = $execute->getUserSession('id');
        $this->paginate = [
            'contain' => ['Users', 'PostLikes', 'PostComments'],
            'conditions' => [
                'Posts.id' => $id,
            ]
        ];
        $postsDetail = $this->paginate($this->Posts);
        $postId = $id;
        $this->set(compact('postsDetail', 'postId'));
    }
}
