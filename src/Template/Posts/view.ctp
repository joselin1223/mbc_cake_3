<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post[]|\Cake\Collection\CollectionInterface $posts
 */
?>
<?= $this->element('navbar'); ?>
<div class="page-wrapper" ng-app="myComment" ng-controller="commentCtrl" data-ng-init="show_comment(<?= h($postId); ?>, 0)">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">View Post</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/posts">Home</a></li>
                        <li class="breadcrumb-item active">View Post</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="card border border-light shadow-sm rounded-top rounded-bottom" style="width: 18rem;">
                    <?php foreach ($postsDetail as $post) { 
                        if ($post['image'] != '') { ?> 
                        <img src="/img/blog/post/<?= h($post['image']) ?>" class="card-img-top" alt="...">
                        <?php } ?>
                        <div class="card-body">
                            <h5 class="card-title"><?= h($post['user']['fullname']) ?></h5>
                            <p class="card-text"><?= h($post['description']) ?></p>
                            <div class="like-comm m-t-20" text-center>
                                <a href="javascript:post().insertLike(<?= $post->id; ?>)" class="link m-r-10 text-primary"><i class="icon-like fa-sm"></i> <small> <span class="like_<?= $post->id; ?>"><?= count($post->post_likes) ?></span> Like</small></a>
                                <a href="#" data-toggle="modal" data-target="#exampleModalCenter" onclick="setParentId(<?= h($post['id']); ?>, <?= h($this->requestAction('/posts/showToBeShare/' .$post['id'], ['return'])); ?>)" class="link m-r-10 text-primary"><i class="icon-action-redo fa-sm"></i> <small>Share</small></a>
                            </div>
                            <span class="sl-date float-lg-right"><small><?= h($post->created); ?></small></span>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card border border-light shadow-sm rounded-top rounded-bottom comment_section" id="comment_section_<?= h($post->id); ?>">
                    <?= $this->element('comment'); ?>
                    <div class="card-body border-top">
                        <nav aria-label="..." class="load-more-button" ng-show="loadButton">
                            <ul class="pagination">
                                <li class="page-item">
                                    <a class="page-link" href="#" ng-click="show_comment(<?= h($post->id); ?>, 3);">Load More</a>
                                </li>
                            </ul>
                        </nav>
                        <div class="row">
                            <div class="col-11">
                                <textarea placeholder="Comment something here" class="form-control" id="comment_description<?= h($post->id); ?>"></textarea>
                                <label class="error-message-comment">Maximum characters are 140 only</label>
                            </div>
                            <div class="col-1 text-right">
                                <button type="button" onclick="comment().add(<?= h($post->id); ?>)" ng-click="show_comment(<?= h($post->id); ?>,1)" class="btn btn-info"><i class="fas fa-paper-plane"></i> </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->element('share_post_modal'); ?>
<?= $this->Html->script('/js/controller/comment_script.js'); ?>
<?= $this->Html->script('/js/controller/post_script.js'); ?>
<script>
    $(document).ready(function() {
        $(`#comment_section_${<?= $postId ?>}`).show();
        $('.close-comment').hide();
    });
</script>