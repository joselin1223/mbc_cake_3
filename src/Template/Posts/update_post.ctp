<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post[]|\Cake\Collection\CollectionInterface $posts
 */
?>
<?= $this->element('navbar'); ?>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Update post</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/posts/">Home</a></li>
                        <li class="breadcrumb-item"><a href="/posts/">NewsFeed</a></li>
                        <li class="breadcrumb-item active">Update post</li>
                    </ol>
                </div>
            </div>
        </div>
        <div style="margin-left:25%; margin-right:25%;">
            <?= $this->element('post_creation'); ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("div").removeClass("col-lg-4");
        $(".col-xlg-3").addClass("col-lg-12");
        $("#submit_post").val('Update post');
    });
</script>