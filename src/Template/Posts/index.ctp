<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post[]|\Cake\Collection\CollectionInterface $posts
 */
?>
<?= $this->element('navbar'); ?>
<div class="page-wrapper" ng-app="myComment" ng-controller="commentCtrl">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Welcome to MicroBlog Chaste</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/posts">Home</a></li>
                        <li class="breadcrumb-item active">NewsFeed</li>
                    </ol>
                </div>
            </div>
        </div>
        <?= $this->element('post_creation'); ?>
        <?= $this->element('post_block'); ?>
        <?= $this->element('share_post_modal'); ?>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#home').addClass('active');
    });
</script>