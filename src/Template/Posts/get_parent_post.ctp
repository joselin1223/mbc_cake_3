<?php $user = $this->Session->read('Auth.User'); ?>
<?php if (isset($parentPost)) { ?>
    <div class="jumbotron">
        <div class="container">
            <div class="profiletimeline" style="margin-top:-6%;margin-bottom:-9%;">
                <div class="sl-item">
                    <div class="sl-left"> <img src="/img/blog/profile/<?= h($parentPost->user->image) ?>" alt="user" class="img-circle" width: 50px; height: 50px; /> </div>
                    <div class="sl-right">
                    <a href="/posts/view/<?= h($parentPost->id) ?>" class="link m-r-10 float-right"><i class="ti-eye fa-sm text-primary"></i></a>
                        <div> <a href="/users/profile/<?= h($parentPost->user->id) ?>" class="link"><?= h($parentPost->user->fullname) ?></a>
                            <div class="m-t-20 row">
                                <?php if ($parentPost->is_deleted == 1) { ?>
                                    <center style="margin-top:-1%;">
                                        <small><b>This post has been deleted by the owner. Sorry for the inconvenience!</b></small>
                                    </center>
                                <?php } else { ?>
                                    <?php if ($parentPost->image != '') { ?>
                                        <div class="col-md-3 col-xs-12"><img src="/img/blog/post/<?= h($parentPost->image) ?>" alt="user" class="img-responsive radius" /></div>
                                    <?php } ?>
                                    <div class="col-md-9 col-xs-12">
                                            <p>
                                                <small><?= h($parentPost->description) ?></small>
                                            </p>
                                    </div>
                                <?php } ?>
                                <div class="like-comm m-t-20">
                                    <span class="sl-date"> <small><?= h($parentPost->created); ?></small></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>