<?php $user = $this->Session->read('Auth.User'); ?>
<div class="col-lg-8 col-xlg-9 col-md-7 shadow-sm p-3 mb-5 bg-white rounded">
    <div class="card">
        <ul class="nav nav-tabs profile-tab" role="tablist">
            <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">NewsFeed</a> </li>
            <li class="mr-auto"></li>
            <li class="nav-item">
                <?php
                if (isset($postPage) || isset($profilePage)) {
                    echo $this->Form->create(
                        'Posts',
                        array(
                            'url' => array('controller' => 'Posts', 'action' => 'index'),
                            'type' => 'delete',
                            'class' => 'app-search d-none d-md-block d-lg-block search_form'
                        )
                    );
                    ?>
                    <input type="text" class="form-control border-dark" placeholder="Search description & enter" name="search_post_description" value="<?= $search ?>">
                <?php
                }
                echo $this->Form->end();
                ?>

            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="home" role="tabpanel">
                <div class="card-body">
                    <div class="profiletimeline">
                        <?php foreach ($postsDetail as $post) { ?>
                            <div class="sl-item">
                                <div class="sl-left"> <img src="/img/blog/profile/<?= h($post->user->image) ?>" alt="user" class="img-circle"/> </div>
                                <?php if ($user['id'] == $post->user->id) { ?>
                                    <a href="javascript:post().delete(<?= $post->id ?>)" class="link m-r-10 float-right"><i class="ti-close fa-sm text-danger"></i></a>
                                    <a href="/posts/updatePost/<?= h($post->id); ?>" class="link m-r-10 float-right"><i class="ti-pencil fa-sm"></i></a>
                                <?php } ?>
                                <div class="sl-right">
                                    <div> <a href="/users/profile/<?= h($post->user->id) ?>" class="link text dark"><?= h($post->user->fullname) ?></a>
                                        <div class="m-t-20 row">
                                            <?php if ($post->image != '') { ?>
                                                <div class="col-md-3 col-xs-12"><img src="/img/blog/post/<?= h($post->image) ?>" alt="user" class="img-responsive radius" /></div>
                                            <?php } ?>
                                            <div class="col-md-9 col-xs-12">
                                                <p> <small><?= h($post->description) ?></small></p>
                                            </div>
                                        </div>
                                        <?php
                                            $toBeShare = $post->id;
                                            if ($post->parent_post_id != null) {
                                                $toBeShare = $post->parent_post_id;
                                                ?>
                                            <?= $this->requestAction('/posts/getParentPost/' . $post->parent_post_id . '/' . $post->id . '/' . $post->user->id, ['return']); ?>
                                        <?php } ?>
                                        <div class="like-comm m-t-20">
                                            <a href="javascript:post().insertLike(<?= $post->id; ?>)" class="link m-r-10 text-primary"><i class="icon-like fa-sm"></i> <small> <span class="like_<?= $post->id; ?>"><?= count($post->post_likes) ?></span> Like</small></a>
                                            <a href="javascript:void(0)" ng-click="show_comment(<?= h($post->id); ?>, 0)" class="link m-r-10 text-primary"><i class="ti-eye fa-sm"></i> <small>comment</small></a>
                                            <a href="#" data-toggle="modal" data-target="#exampleModalCenter" onclick="setParentId(<?= $toBeShare; ?>, <?= h($this->requestAction('/posts/showToBeShare/' . $toBeShare, ['return'])); ?>);" class="link m-r-10 text-primary"><i class="icon-action-redo fa-sm"></i> <small>Share</small></a>
                                            <span class="sl-date float-lg-right"><small><?= h($post->created); ?></small></span>
                                        </div>
                                        <div class="card border border-light shadow-sm rounded-top rounded-bottom comment_section" id="comment_section_<?= h($post->id); ?>">
                                            <?= $this->element('comment'); ?>
                                            <div class="card-body border-top">
                                                <nav aria-label="..." class="load-more-button">
                                                    <ul class="pagination">
                                                        <li class="page-item">
                                                            <a class="page-link" href="#comment_section_<?= h($post->id); ?>" ng-click="show_comment(<?= h($post->id); ?>, 3)">Load More</a>
                                                        </li>
                                                    </ul>
                                                </nav>
                                                <div class="row">
                                                    <div class="col-11">
                                                        <textarea placeholder="Comment something here" class="form-control" id="comment_description<?= h($post->id); ?>"></textarea>
                                                        <label class="error-message-comment">Maximum characters are 140 only</label>
                                                    </div>
                                                    <div class="col-1 text-right">
                                                        <button type="button" onclick="comment().add(<?= h($post->id); ?>)" ng-click="show_comment(<?= h($post->id); ?>,1)" class="btn btn-info"><i class="fas fa-paper-plane"></i> </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        <?php } ?>
                        <center>
                            <?php if ($search != '') { ?>
                                <p>
                                    Searched result for the name and post description with '<?= h($search); ?>'
                                </p>
                            <?php } ?>
                            <div class="paginator">
                                <ul class="pagination_post">
                                    <?= $this->Paginator->first('<< ' . __('First ')) ?>
                                    <?php if ($this->Paginator->hasPrev()) {
                                        echo $this->Paginator->prev('< ' . __('Previous '));
                                    } ?>
                                    <?= $this->Paginator->numbers() ?>
                                    <?php
                                    if ($this->Paginator->hasNext()) {
                                        echo $this->Paginator->next(__('Next') . ' >');
                                    }
                                    ?>
                                    <?= $this->Paginator->last(__('Last') . ' >>') ?>
                                </ul>
                                <p><small><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></small></p>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
<?= $this->Html->script('/js/controller/comment_script.js'); ?>
<?= $this->Html->script('/js/controller/post_script.js'); ?>