<?php $user = $this->Session->read('Auth.User'); ?>
<div class="card shadow-sm p-3 bg-white rounded">
    <div class="card-body">
        <?= $this->requestAction('/users/checkIfFollowed/' . $profilePage['id'], ['return']); ?>
        <center class="m-t-30"> <img src="/img/blog/profile/<?= $profilePage['image'] ?>" class="img-circle" width="150" />
            <h4 class="card-title m-t-10"><?= h($profilePage['fullname']); ?></h4>
            <div class="row text-center justify-content-md-center">
                <div class="col-4">
                    <a class="link" href="/users/following/<?= $profilePage['id'] ?>"><i class=" icon-user-following text-primary"> Following </i>
                        <font class="font-medium text-primary"><?= $totalFollower; ?></font>
                    </a>
                </div>
                <div class="col-4">
                    <a class="link" href="/users/follower/<?= $profilePage['id'] ?>"><i class="icon-user-follow text-primary"> Follower </i>
                        <font class="font-medium text-primary"><?= $totalFollowing; ?></font>
                    </a>
                </div>
            </div>
            <br>
            <small class="text-muted"><?= h($profilePage['bio']); ?></small>
        </center>
    </div>
    <div>
        <hr>
    </div>
    <div class="card-body">
        <small class="text-muted">Email address </small>
        <h6><?= h($profilePage['email']); ?></h6>
        <small class="text-muted p-t-30 db">Age</small>
        <h6><?= h($profilePage['age']); ?></h6>
        <small class="text-muted p-t-30 db">Address</small>
        <h6><?= h($profilePage['address']); ?></h6>
    </div>
</div>