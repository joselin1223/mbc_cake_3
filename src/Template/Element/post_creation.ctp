<?php $user = $this->Session->read('Auth.User'); ?>
<div class="row">
    <div class="col-lg-4 col-xlg-3 col-md-5">
        <?php if (isset($profilePage)) { ?>
            <?= $this->element('profile_block_info'); ?>
        <?php } ?>
        <?php if (isset($profilePage) && $profilePage['id'] == $user['id'] || isset($postPage) || isset($updatePost)) { ?>
            <?php
                $imgSrc = false;
                if (isset($updatePost)) {
                    $imgSrc = "/img/blog/post/" . h(h($posts['image']));
                    if ($posts['image'] == '') {
                        $imgSrc = '';
                    }
                }
                ?>
            <div class="card sticky-top shadow-sm bg-white rounded">
                <div class="card-body">
                    <?= $this->Form->create(
                            $posts,
                            [
                                'type' => 'post',
                                'enctype' => 'multipart/form-data',
                                'class' => 'floating-labels m-t-40',
                                'onsubmit' => 'disableField()'
                            ]
                        ) ?>
                    <div class="form-group">
                        <?= $this->Form->control(
                                'description',
                                [
                                    'class' => 'form-control has-success m-b-40',
                                    'type' => 'textarea',
                                    'rows' => '5',
                                    'label' => 'What are you thinking?'
                                ]
                            ) ?>
                        <?php if ($imgSrc != '' && isset($updatePost)) { ?>
                            <a class="float-right" href="/posts/removeImage/<?= $posts->id ?>"><button type="button" class="btn btn-danger"> Remove Image</button> </a>
                        <?php } ?>
                        <?= $this->Form->control(
                                'image',
                                [
                                    'type' => 'file',
                                    'id' => 'input-file-now',
                                    'class' => 'dropify',
                                    'data-max-file-size' => '2M',
                                    'label' => false,
                                    'data-default-file' => $imgSrc
                                ]
                            ) ?>
                        <br>
                        <div class="container text-center">
                            <div class="row align-items-start">
                                <div class="col">
                                    <div class="row button-group">
                                        <div class="col-lg-12">
                                            <?= $this->Form->control('Submit Post', [
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-rounded btn-block btn-outline-success submit',
                                                    'label' => false,
                                                    'id' => 'submit_post'
                                                ]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        <?php } ?>
    </div>
    <?= $this->Html->script('http://jeremyfagis.github.io/dropify/dist/js/dropify.min.js') ?>
    <?= $this->Html->css('http://jeremyfagis.github.io/dropify/dist/css/dropify.min.css') ?>
    <script>
        $(document).ready(function() {
            // Basic
            var s;
            $('.dropify').dropify({
                allowedFileExtensions: ["gif", "GIF", "jpeg", "JPEG", "png", "PNG", "jpg", "JPG"]
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });
            s

            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>