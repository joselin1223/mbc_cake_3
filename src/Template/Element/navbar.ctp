<?php
$user = $this->Session->read('Auth.User');
?>
<?= $this->Html->script('/js/sidebarmenu.js') ?>
<div id="nav_bar">
    <header class="topbar" style="background-color: #bdc1c9">
        <nav class="navbar top-navbar navbar-expand-md navbar-dark">
            <div class="navbar-collapse">
                <a class="navbar-brand" href="/posts">
                    <img src="/img/logo_auth1.png" alt="homepage" class="light-logo" width="60" />
                </a>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <?php
                        echo $this->Form->create(
                            'Users',
                            array(
                                'url' => array('controller' => 'Followers', 'action' => 'index'),
                                'type' => 'delete',
                                'class' => 'app-search d-none d-md-block d-lg-block'
                            )
                        );
                        $userSearch = '';
                        if (isset($searchUser)) {
                            $userSearch = $searchUser;
                        } ?>
                        <input type="text" class="form-control" placeholder="Search & enter" name="search_user" value="<?= $userSearch ?>">
                        <?php
                        echo $this->Form->end();
                        ?>

                    </li>
                </ul>
                <ul class="navbar-nav my-lg-0">
                    <li class="nav-item dropdown u-pro">
                        <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/img/blog/profile/<?= h($user['image']) ?>" alt="user"> <span class="text-dark"> <?= h($user['fullname']) ?> &nbsp;<i class="fa fa-angle-down"></i></span> </a>
                        <div class="dropdown-menu dropdown-menu-right animated flipInY">
                            <a href="/users/profile/<?= h($user['id']) ?>" class="dropdown-item"><i class="ti-user"></i> My Profile</a>
                            <a href="/users/update-user" class="dropdown-item"><i class="ti-user"></i> Update Account</a>
                            <a href="/users/update-profile-pic" class="dropdown-item"><i class="ti-user"></i> Change Profile Pic</a>
                            <a href="/users/change-password" class="dropdown-item"><i class="ti-user"></i> Change password</a>
                            <a href="/users/logout" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>

                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
</div>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/posts">MicroBlogChaste</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item" id="home">
                <a class="nav-link" href="/posts">Home</a>
            </li>
            <li class="nav-item" id="follow">
                <a class="nav-link" href="/followers">Follow Someone</a>
            </li>
        </ul>
    </div>
</nav>