                                            <div class="card-body">
                                                <small><span class="text-dark">Enter comment here:</span></small>
                                                <a href="javascript:void(0)" class="link m-r-10 float-right"><i class="ti-close fa-sm text-danger close-comment"></i></a>
                                                <div class="chat-box" id="chat" style="position: relative; max-height:280px; overflow: auto;">
                                                    <ul class="chat-list">
                                                        <li ng-repeat="comment in data">
                                                            <div class="chat-img"><img ng-src="/img/blog/profile/{{comment.user.image}}" alt="user" width: 50px; height: 50px;></div>
                                                            <div class="chat-content">
                                                                <h5><small>{{ comment.user.fullname }}</small></h5>
                                                                <div class="box bg-light-info"><small>{{ comment.description }}</small></div>
                                                            </div>
                                                            <div class="chat-time"><small>{{ comment.created | date:'M/d/yy, h:mm a' }}</small></div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>