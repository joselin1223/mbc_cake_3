<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Share post</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->Form->create(
                    'Posts',
                    [
                        'type' => 'post',
                        'enctype' => 'multipart/form-data',
                        'class' => 'floating-labels m-t-40',
                        'id' => 'share-post',
                        'action' => '/sharePost/',
                        'onsubmit' => 'disableField()'
                    ]
                ) ?>
                <?= $this->Form->control('parent_post_id', [
                    'class' => 'form-control',
                    'type' => 'hidden',
                    'id' => 'parent_post_id',
                    'label' => false
                ]) ?>
                <div class="form-group">
                    <?= $this->Form->control(
                        'update_description',
                        [
                            'class' => 'form-control has-success m-b-40',
                            'type' => 'textarea',
                            'rows' => '5',
                            'required' => "required",
                            'label' => 'What are you thinking about this post?'
                        ]
                    ) ?>
                    <div class="container text-center">
                        <div class="row align-items-start">
                            <div class="col">
                                <div class="row button-group">
                                    <div class="col-lg-12">
                                        <?= $this->Form->control('Submit Post', [
                                            'type' => 'submit',
                                            'class' => 'btn btn-rounded btn-block btn-outline-success submit',
                                            'label' => false,
                                            'id' => 'submit_post'
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
                <div class="jumbotron">
                    <div class="sl-left"><span id="post_owner_image"></span><span id="post_owner_fullname"></span></div>
                    <div class="sl-right">
                        <div class="m-t-20 row">
                            <div class="col-md-3 col-xs-12 post_shared_image" id="post_shared_image"></div>
                            <div class="col-md-9 col-xs-12">
                                <p> <small id="post_shared_description"></small></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    function setParentId(id, parent_data) {
        $('#parent_post_id').val(id);
        $('#share-post').attr('action', '/posts/sharePost/' + id);
        $('#post_shared_image').html(``);
        $('#post_shared_description').text(parent_data.description);
        $('#post_owner_image').html(`<img src="/img/blog/profile/${parent_data.user.image}" alt="user" class="img-circle" width="50" height="50"; />`);
        $('#post_owner_fullname').text(parent_data.user.fullname);
        $('.post_shared_image').hide();
        if (parent_data.image != '') {
            $('.post_shared_image').show();
            $('#post_shared_image').html(`<img src="/img/blog/post/${parent_data.image}" alt="user" class="img-responsive radius" />`);
        }
    }
</script>