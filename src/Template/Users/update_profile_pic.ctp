<?= $this->element('navbar'); ?>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Update Account</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/posts">Home</a></li>
                        <li class="breadcrumb-item"><a href="/users/profile/<?= $userUsed['id'] ?>">Profile</a></li>
                        <li class="breadcrumb-item active">Profile Picture</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- Row -->
        <div class="row">
            <div class="col-lg-12 shadow-sm p-3 mb-5 bg-white rounded">
                <div class="card">
                    <div class="card-body">
                        <?= $this->Form->create(
                            $userUsed,
                            [
                                'type' => 'post',
                                'class' => 'form-horizontal form-material',
                                'enctype' => 'multipart/form-data',
                                'onsubmit' => 'disableField()'
                            ]
                        ) ?>
                        <div class="form-body">
                            <h3 class="card-title">Profile Picture</h3>
                            <hr>
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php if ($userUsed['image'] != 'profile.png') { ?>
                                            <a class="float-right" onclick="disableField()" href="/users/removeProfileImage/"><button type="button" class="btn btn-danger"> Remove Image</button> </a>
                                        <?php } ?>
                                        <?= $this->Form->control('image', [
                                            'type' => 'file',
                                            'id' => 'input-file-now',
                                            'class' => 'dropify',
                                            'data-max-file-size' => '2M',
                                            'data-default-file' => "/img/blog/profile/" . h(h($userUsed['image']))
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                            <a href="/users/profile/<?= $userUsed['id'] ?>"><button type="button" class="btn btn-inverse">Cancel</button></a>
                        </div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->
    </div>
</div>
<?= $this->Html->script('http://jeremyfagis.github.io/dropify/dist/js/dropify.min.js') ?>
<?= $this->Html->css('http://jeremyfagis.github.io/dropify/dist/css/dropify.min.css') ?>
<script>
    $(document).ready(function() {
        // Basic
        var s;
        $('.dropify').dropify({
            allowedFileExtensions: ["gif", "GIF", "jpeg", "JPEG", "png", "PNG", "jpg", "JPG"]
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        s

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>