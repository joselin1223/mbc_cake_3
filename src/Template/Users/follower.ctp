<?= $this->element('navbar'); ?>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Follower</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/posts">Home</a></li>
                        <li class="breadcrumb-item"><a href="/users/profile/<?= $userOwner; ?>">Profile</a></li>
                        <li class="breadcrumb-item active">Follower</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="mr-auto"></li>
        <li class="nav-item">
            <?php
            echo $this->Form->create(
                'Posts',
                array(
                    'url' => array('controller' => 'Users', 'action' => 'follower', $userOwner),
                    'type' => 'delete',
                    'class' => 'app-search d-none d-md-block d-lg-block search_form'
                )
            );
            ?>
            <input type="text" class="form-control border-dark" placeholder="Search & enter" name="search_follower" value="<?= h($search) ?>">
            <?php
            echo $this->Form->end();
            ?>
        </li>
    </ul>
    <div class="row">
        <?php
        $userSession = $this->Session->read('Auth.User');
        foreach ($userFollowing as $user) { ?>
            <div class="col-md-3">
                <div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="false">
                    <div class="toast-header">
                        <img src="/img/blog/profile/<?= h($user->image) ?>" class="rounded mr-2" height="20">
                        <strong class="mr-auto"><a href="/users/profile/<?= h($user->id) ?>"><?= h($user->fullname) ?></a></strong>
                        <small><?= date('F, Y', strtotime(h($user->created))) ?></small>
                    </div>
                    <?php
                        $followed = 'Follow';
                        for ($j = 0; $j < count($user->followers); $j++) {
                            if ($userSession['id'] == $user->followers[$j]['user_id_from']) {
                                $followed = 'Unfollow';
                            }
                        }
                        ?>
                    <div class="toast-body text-center">

                    </div>
                </div>
            </div>
            <br><br><br><br><br>
        <?php } ?>
    </div>
    <center>
        <?php if ($search != '') { ?>
            <p>
                Search result for the name with '<?= h($search); ?>'
            </p>
        <?php } ?>
        <div class="paginator">
            <ul class="pagination_post">
                <?= $this->Paginator->first('<< ' . __('First ')) ?>
                <?php if ($this->Paginator->hasPrev()) {
                    echo $this->Paginator->prev('< ' . __('Previous '));
                } ?>
                <?= $this->Paginator->numbers() ?>
                <?php
                if ($this->Paginator->hasNext()) {
                    echo $this->Paginator->next(__('Next') . ' >');
                }
                ?>
                <?= $this->Paginator->last(__('Last') . ' >>') ?>
            </ul>
            <p><small><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></small></p>
        </div>
    </center>
</div>
<script>
    $(function() {
        $('.toast').toast('show');
    });
</script>
<script>
    $(function() {})

    function follow(userIdTo) {
        data = {
            user_id_to: userIdTo
        };
        $(`.follow-button-${userIdTo}`).hide();
        $.ajax({
            data: data,
            type: 'post',
            url: '/followers/addFollow',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', '<?= $this->request->getParam('_csrfToken') ?>');
            },
            success: function(success) {
                if (success == 'followed') {
                    $(`.follow-text-${userIdTo}`).html('Unfollow');
                }
                if (success == 'unfollowed') {
                    $(`.follow-text-${userIdTo}`).html('Follow');
                }
                $(`.follow-button-${userIdTo}`).show();
            }
        });
    }
</script>