<?php
$userSession = $this->Session->read('Auth.User');
if ($userSession['id'] != $visitedUser) {
    ?>
    <?php if ($userCount > 0) { ?>
        <button type="button" class="btn btn-danger float-right follow-button" value="<?= $visitedUser ?>"><i class="icon-user-unfollow"></i> <span class="f-txt">Unfollow</span></button><br>
    <?php } else { ?>
        <button type="button" class="btn btn-success float-right follow-button" value="<?= $visitedUser ?>"><i class="icon-user-follow"></i> <span class="f-txt">Follow</span></button><br>
<?php }
} ?>