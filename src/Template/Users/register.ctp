<section id="wrapper" style="background-image:url(/img/back.jpg); background-size: cover;">
    <div class="card" style="margin-left: 73.8%;">
        <div class="card-body">
            <?= $this->Form->create(
                $user,
                [
                    'type' => 'post',
                    'class' => 'form-horizontal form-material',
                    'id' => 'loginform',
                    'enctype' => 'multipart/form-data',
                    'onsubmit' => 'disableField()'
                ]
            ) ?>
            <div class="text-center">
                <a href="javascript:void(0)" class="db"><img src="/img/logo_auth1.png" alt="Home" height="150" /></a>
                <h3 class="box-title m-t-40 m-b-0">Register Now</h3><small>Create your account and enjoy</small>
            </div>
            <div class="form-group m-t-20">
                <div class="col-xs-12">
                    <?= $this->Form->control('username', [
                        'class' => 'form-control',
                        'type' => 'text',
                    ]) ?>
                </div>
                <br>
                <div class="col-xs-12">
                    <?= $this->Form->control('password', [
                        'class' => 'form-control',
                        'type' => 'password',
                    ]) ?>
                </div>
                <br>
                <div class="col-xs-12">
                    <?= $this->Form->control('confirm_password', [
                        'class' => 'form-control',
                        'type' => 'password',
                    ]) ?>
                </div>
                <br>
                <div class="col-xs-12">
                    <?= $this->Form->control('fullname', [
                        'class' => 'form-control',
                        'type' => 'text',
                    ]) ?>
                </div>
                <br>
                <div class="col-xs-12">
                    <?= $this->Form->control('age', [
                        'class' => 'form-control',
                        'type' => 'number',
                    ]) ?>
                </div>
                <br>
                <div class="col-xs-12">
                    <?= $this->Form->control('address', [
                        'class' => 'form-control',
                        'type' => 'text',
                    ]) ?>
                </div>
                <br>
                <div class="col-xs-12">
                    <?= $this->Form->control('email', [
                        'class' => 'form-control',
                        'type' => 'email',
                    ]) ?>
                </div>
                <br>
                <div class="col-xs-12">
                    <?= $this->Form->control('bio', [
                        'class' => 'form-control',
                        'type' => 'textarea',
                        'rows' => 3
                    ]) ?>
                </div>
                <br>
                <div class="col-xs-12">
                    <?= $this->Form->control('image', [
                        'type' => 'file',
                        'id' => 'input-file-now',
                        'class' => 'dropify',
                        'data-max-file-size' => '2M',
                    ]) ?>
                </div>
            </div>
            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" style="background-color: rgb(52, 54, 53);">Sign Up</button>
                </div>
            </div>
            <div class="form-group m-b-0">
                <div class="col-sm-12 text-center">
                    <p>Already have an account? <a href="/" class="text-info m-l-5"><b>Sign In</b></a></p>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div>
</section>
<script>
    $(function() {
        $('.footer').hide();
    });
</script>
<?= $this->Html->script('http://jeremyfagis.github.io/dropify/dist/js/dropify.min.js') ?>
<?= $this->Html->css('http://jeremyfagis.github.io/dropify/dist/css/dropify.min.css') ?>
<script>
    $(document).ready(function() {
        // Basic
        var s;
        $('.dropify').dropify({
            allowedFileExtensions: ["gif", "GIF", "jpeg", "JPEG", "png", "PNG", "jpg", "JPG"]
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        s

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>