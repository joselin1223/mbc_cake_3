<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post[]|\Cake\Collection\CollectionInterface $posts
 */
?>
<?= $this->element('navbar'); ?>
<div class="page-wrapper" ng-app="myComment" ng-controller="commentCtrl">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Profile</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/posts">Home</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div>
            </div>
        </div>
        <?= $this->element('post_creation'); ?>
        <?= $this->element('post_block'); ?>
        <?= $this->element('share_post_modal'); ?>
    </div>
</div>

<script>
    $(function() {
        $('.search_form').attr('action', '/users/profile/' + <?= h($profilePage['id']); ?>);
    });
</script>
<script>
    $(function() {
        $('.follow-button').click(function(e) {
            let id = $(this).val();
            $(this).hide();
            follow(id);
        });
    })

    function follow(userIdTo) {
        data = {
            user_id_to: userIdTo
        };
        $.ajax({
            data: data,
            type: 'post',
            url: '/followers/addFollow',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', '<?= $this->request->getParam('_csrfToken') ?>');
            },
            success: function(success) {
                $(location).attr('href', '/users/profile/' + <?= h($profilePage['id']); ?>);
            }
        });
    }
</script>