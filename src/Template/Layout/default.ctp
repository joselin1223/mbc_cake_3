<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->script('https://code.jquery.com/jquery-3.4.1.min.js') ?>
    <?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js') ?>
    <?= $this->Html->script('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js') ?>
    <?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/node-waves/0.7.6/waves.min.js') ?>
    <?= $this->Html->script('https://cdn.jsdelivr.net/npm/sweetalert2@8'); ?>
    <?= $this->Html->script('https://ajax.googleapis.com/ajax/libs/angularjs/1.7.8/angular.min.js'); ?>
    <?= $this->Html->script('/js/custom.min.js') ?>

    <?= $this->Html->css('/css/dist/css/style.min.css') ?>
    <?= $this->Html->css('/css/my_custom_style.css'); ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>

<body class="horizontal-nav skin-default fixed-layout default-theme">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Loading</p>
        </div>
    </div>
    <?php echo $this->Flash->render(); ?>
    <?php echo $this->fetch('content'); ?>

</body>

<footer class="footer">
    © 2019 Microblog Chaste -Joselin T. Macayanan
</footer>
<script type="text/javascript">
    function disableField() {
        $("input").prop("readonly", true);
        $("button").prop("disabled", true);
        $(".submit").prop("disabled", true);
    }
    $(function() {
        $(".preloader").fadeOut();
    });
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    });
</script>

</html>