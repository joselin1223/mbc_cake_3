<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post[]|\Cake\Collection\CollectionInterface $posts
 */
?>
<?= $this->element('navbar'); ?>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Follow Someone</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Follow</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- start -->
        <div class="row">
            <?php
            $userSession = $this->Session->read('Auth.User');
            foreach ($userDetail as $user) { ?>
                <div class="col-md-3">
                    <div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="false">
                        <div class="toast-header">
                            <img src="/img/blog/profile/<?= h($user->image) ?>" class="rounded mr-2" height="20">
                            <strong class="mr-auto"><a href="/users/profile/<?= h($user->id) ?>"><?= h($user->fullname) ?></a></strong>
                            <small><?= date('F, Y', strtotime(h($user->created))) ?></small>
                        </div>
                        <?php
                            $followed = 'Follow';
                            for ($j = 0; $j < count($user->followers); $j++) {
                                if ($userSession['id'] == $user->followers[$j]['user_id_from']) {
                                    $followed = 'Unfollow';
                                }
                            }
                            ?>
                        <div class="toast-body text-center">
                            <button type="button" class="btn btn-secondary btn-outline follow-button-<?= h($user->id) ?>" onclick="follow(<?= h($user->id) ?>)" value="<?= h($user->id) ?>">
                                <i class="icon-user-follow text-primary"></i>
                                <span class="text follow-text-<?= h($user->id) ?> text-primary"><?= $followed; ?></span>
                            </button>
                        </div>
                    </div>
                </div>
                <br><br><br><br><br>
            <?php } ?>
        </div>
        <br><br><br><br><br>
        <center>
            <?php if (isset($searchUser) && $searchUser != '') { ?>
                <p>
                    Search result for the name with '<?= h($searchUser); ?>'
                </p>
            <?php } ?>
            <div class="paginator">
                <ul class="pagination_post">
                    <?= $this->Paginator->first('<< ' . __('First ')) ?>
                    <?php if ($this->Paginator->hasPrev()) {
                        echo $this->Paginator->prev('< ' . __('Previous '));
                    } ?>
                    <?= $this->Paginator->numbers() ?>
                    <?php
                    if ($this->Paginator->hasNext()) {
                        echo $this->Paginator->next(__('Next') . ' >');
                    }
                    ?>
                    <?= $this->Paginator->last(__('Last') . ' >>') ?>
                </ul>
                <p><small><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></small></p>
            </div>
        </center>
        <!-- end -->

    </div>
</div>
<script>
    $(function() {
        $('.toast').toast('show');
        $('#follow').addClass('active');
    });
</script>
<script>
    $(function() {})

    function follow(userIdTo) {
        data = {
            user_id_to: userIdTo
        };
        $(`.follow-button-${userIdTo}`).hide();
        $.ajax({
            data: data,
            type: 'post',
            url: '/followers/addFollow',
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', '<?= $this->request->getParam('_csrfToken') ?>');
            },
            success: function(success) {
                if (success == 'followed') {
                    $(`.follow-text-${userIdTo}`).html('Unfollow');
                }
                if (success == 'unfollowed') {
                    $(`.follow-text-${userIdTo}`).html('Follow');
                }
                $(`.follow-button-${userIdTo}`).show();
            }
        });
    }
</script>