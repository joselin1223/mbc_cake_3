    $(function() {
        $('.comment_section').hide();
        $('.close-comment').click(function (e) { 
            $('.comment_section').hide();
            $('.error-message-comment').hide();
        });
    });

    //------------------------------- Beginning Comment Functions ---------------------------------------------/
    var app = angular.module('myComment', []);
    var item = 3;
    var tempData = [];
    var firstTime = true;
    app.controller('commentCtrl', function($scope, $http) {
        $scope.loadButton = true;
        $scope.show_comment = function(post_id, add) {
            data = {
                'post_id': post_id,
            }
            item +=add;
            $.ajax({
                data: data,
                type: 'post',
                url: `/postComments/viewComments/${item}`,
                async: false,
                beforeSend: function(xhr) { // Add this line
                    xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                },
                success: function(response) {
                    let commentData = JSON.parse(response);
                    $('.comment_section').hide();
                    $(`#comment_section_${post_id}`).show();
                    if (commentData.length == tempData.length && !(firstTime) && add != 0) {
                        $('.load-more-button').hide();
                    }
                    firstTime = false;
                    tempData = commentData;
                    $scope.data = commentData;
                }
            });
        };
    });

    function comment() {
        return {
            add: function(post_id) {
                data = {
                    post_id: post_id,
                    description: $(`#comment_description${post_id}`).val()
                }
                try {
                    $.ajax({
                        data: data,
                        type: 'post',
                        url: '/postComments/add/',
                        beforeSend: function(xhr) { // Add this line
                            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                        },
                        async: false,
                        success: function(success) {
                            if (success == true) {
                                $('.error-message-comment').hide();
                                $(`#comment_description${post_id}`).val('');
                            } else {
                                $('.error-message-comment').html(success);
                                $('.error-message-comment').show();
                            }
                        }
                    });
                } catch (error) {
                }
            }
        }
    }
    //------------------------------- End Comment Functions ---------------------------------------------/