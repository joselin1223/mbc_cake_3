    //--------------------------------- Beginning POST Functions --------------------------------------/
    function post() {
        return {
            delete: function(id) {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#6c757d',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        data = {
                            'id': id
                        }
                        $.ajax({
                            data: data,
                            type: 'post',
                            url: '/posts/delete',
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                            },
                            success: function(response) {
                                let redirect = false;
                                if (response.status) {
                                    message().delete_success();
                                    redirect = true;
                                } else {
                                    message().failed();
                                    redirect = false;
                                }
                                if (response.status == 'invalid') {
                                    message().bad();
                                    redirect = false;
                                }
                                if (redirect) {
                                    setTimeout(function() {
                                        $(location).attr('href', '/posts');
                                    }, 500);
                                }
                            }
                        });
                    }
                });
            },
            insertLike: function(post_id) {
                data = {
                    'post_id': post_id
                }
                try {
                    $.ajax({
                        data: data,
                        type: 'post',
                        url: '/postLikes/insertLike',
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                        },
                        success: function(response) {
                            $(`.like_${post_id}`).html(` ${response}`);
                        },
                    });
                } catch (error) {

                }
            }
        }
    }
    //---------------------------------- End POST Functions ------------------------------------------/

    //------------------------------ Beginning Messages Functions -------------------------------------/
    function message() {
        return {
            delete_success: function() {
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            },
            failed: function() {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    footer: 'Please Try again later!!'
                })
            },
            bad: function() {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'You are trying to delete other user properties',
                    footer: 'Please try again. Thank you!'
                })
            }
        }
    }
    //---------------------------------- End Messages Functions -------------------------------------------/