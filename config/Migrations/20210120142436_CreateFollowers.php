<?php

use Migrations\AbstractMigration;

class CreateFollowers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table(
            'followers',
            [
                'collation' => 'utf8_general_ci'
            ]
        );
        $table->addColumn(
            'from_to',
            'string',
            [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ]
        )->addIndex(
            [
                'from_to',
            ],
            [
                'name' => 'UNIQUE_FROM_TO',
                'unique' => true,
            ]
        );
        $table->addColumn(
            'user_id_from',
            'integer',
            [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ]
        );
        $table->addColumn(
            'user_id_to',
            'integer',
            [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ]
        );
        $table->addColumn(
            'is_deleted',
            'integer',
            [
                'default' => 0,
                'limit' => 1,
                'null' => false,
            ]
        );
        $table->addColumn(
            'modified',
            'timestamp',
            [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false,
            ]
        );
        $table->addColumn(
            'created',
            'timestamp',
            [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false,
            ]
        );
        $table->create();
    }
}
