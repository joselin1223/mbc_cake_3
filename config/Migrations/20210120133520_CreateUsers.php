<?php

use Migrations\AbstractMigration;

class CreateUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table(
            'users',
            [
                'collation' => 'utf8_general_ci'
            ]
        );
        $table->addColumn(
            'username',
            'string',
            [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ]
        );
        $table->addColumn(
            'password',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ]
        );
        $table->addColumn(
            'change_password_activation',
            'string',
            [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ]
        );
        $table->addColumn(
            'password_to_be_use',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'fullname',
            'string',
            [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ]
        );
        $table->addColumn(
            'age',
            'integer',
            [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ]
        );
        $table->addColumn(
            'address',
            'string',
            [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ]
        );
        $table->addColumn(
            'email',
            'string',
            [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ]
        );
        $table->addColumn(
            'image',
            'text',
            [
                'default' => null,
                'null' => true,
            ]
        );
        $table->addColumn(
            'bio',
            'string',
            [
                'default' => null,
                'limit' => 140,
                'null' => false,
            ]
        );
        $table->addColumn(
            'activation_code',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        );
        $table->addColumn(
            'activated',
            'integer',
            [
                'default' => 0,
                'limit' => 1,
                'null' => true,
            ]
        );
        $table->addColumn(
            'modified',
            'timestamp',
            [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false,
            ]
        );
        $table->addColumn(
            'created',
            'timestamp',
            [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false,
            ]
        );
        $table->create();
    }
}
