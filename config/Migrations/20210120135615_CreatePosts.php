<?php

use Migrations\AbstractMigration;

class CreatePosts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table(
            'posts',
            [
                'collation' => 'utf8_general_ci'
            ]
        );
        $table->addColumn(
            'user_id',
            'integer',
            [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ]
        );
        $table->addColumn(
            'parent_post_id',
            'integer',
            [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ]
        );
        $table->addColumn(
            'description',
            'string',
            [
                'default' => null,
                'limit' => 140,
                'null' => false,
            ]
        );
        $table->addColumn(
            'image',
            'text',
            [
                'default' => null,
                'null' => false,
            ]
        );
        $table->addColumn(
            'is_deleted',
            'integer',
            [
                'default' => 0,
                'limit' => 1,
                'null' => false,
            ]
        );
        $table->addColumn(
            'modified',
            'timestamp',
            [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false,
            ]
        );
        $table->addColumn(
            'created',
            'timestamp',
            [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false,
            ]
        );
        $table->create();
    }
}
