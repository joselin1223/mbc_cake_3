<?php

use Migrations\AbstractMigration;

class CreatePostLikes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table(
            'post_likes',
            [
                'collation' => 'utf8_general_ci'
            ]
        );
        $table->addColumn(
            'post_user',
            'string',
            [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ]
        )->addIndex(
            [
                'post_user',
            ],
            [
                'name' => 'UNIQUE_POST_USER',
                'unique' => true,
            ]
        );
        $table->addColumn(
            'post_id',
            'integer',
            [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ]
        );
        $table->addColumn(
            'user_id',
            'integer',
            [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ]
        );
        $table->addColumn(
            'is_deleted',
            'integer',
            [
                'default' => 0,
                'limit' => 1,
                'null' => false,
            ]
        );
        $table->addColumn(
            'modified',
            'timestamp',
            [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false,
            ]
        );
        $table->addColumn(
            'created',
            'timestamp',
            [
                'default' => 'CURRENT_TIMESTAMP',
                'null' => false,
            ]
        );
        $table->create();
    }
}
